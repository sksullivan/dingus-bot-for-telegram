import requests
import time
import urllib

YOUTUBE_VIDEO_BASE_URL = "https://www.youtube.com/watch?v="
SEARCH_URL = "https://www.googleapis.com/youtube/v3/search"
SECRET = "AIzaSyBf62CnfAkrgWNphcxzbubneCkdT0hO0fc"
BOT_SECRET = "117653958:AAFjsr-6lQkUlXfUpVKZ4CXdFJS1Vtquojs"

class RequestMaker():
    def __init__(self, lastUpdateID):
        self.lastID = lastUpdateID

    def run(self):
        print('run')
        reply = requests.get("https://api.telegram.org/bot{}/getUpdates?offset={}".format(BOT_SECRET,self.lastID))
        updates = reply.json()['result']
        if (len(updates) > 0):
            for update in updates:
                print(update)
                if 'text' in update['message']:
                    chatID = update['message']['chat']['id']
                    QUERY = update['message']['text'][1:]

                    search_results = requests.get("{}?part=snippet&q={}&key={}".format(SEARCH_URL,QUERY,SECRET))
                    
                    i = 0
                    while (not 'videoId' in search_results.json()['items'][i]['id']):
                        i+=1
                    video_id = search_results.json()['items'][i]['id']['videoId']
                    resp = "{}{}".format(YOUTUBE_VIDEO_BASE_URL,video_id)
                    requests.get("https://api.telegram.org/bot{}/sendMessage?chat_id={}&text={}".format(BOT_SECRET,chatID,resp))
                    self.lastID = update['update_id'] if update['update_id'] > self.lastID else self.lastID
            self.lastID += 1
        print(self.lastID)
        time.sleep(10)
        return self.lastID

if __name__ == '__main__':
    print("starting")
    lastID = 0
    while (1):
        req = RequestMaker(lastID)
        lastID = req.run()
        

